import Test.QuickCheck

main :: IO ()
main = undefined

data Arbre coul val = Feuille | Noeud coul val (Arbre coul val) (Arbre coul val)
    deriving Show

mapArbre :: (coul->coul')->(val->val')->Arbre coul val -> Arbre coul' val'
mapArbre _ _ Feuille = Feuille
mapArbre f1 f2 (Noeud coul val g d) = Noeud (f1 coul) (f2 val) (mapArbre f1 f2 g) (mapArbre f1 f2 d)

hauteur :: Arbre coul val-> Int
hauteur Feuille = 0
hauteur (Noeud _ _ gauche droite) = 1 + max (hauteur gauche) (hauteur droite)

taille ::Arbre coul val-> Int
taille Feuille = 0
taille (Noeud _ _ gauche droite) = 1 + taille gauche + taille droite


dimension :: (Int->Int->Int)->Arbre coul val-> Int
dimension _ Feuille = 0 
dimension f (Noeud _ _ gauche droite) = 1 + f g d where
    g = dimension f gauche
    d = dimension f droite

peigneGauche :: [(c,a)] -> Arbre c a
peigneGauche [] = Feuille
peigneGauche ( (c, v):xs ) = (Noeud c v (peigneGauche xs) Feuille)

-- la fonction teste si la hauteur de l'arbre est egale a length de xs
prop_hauteurPeigne xs = length xs == hauteur (peigneGauche xs)

dimensionGeneralisee :: (a-> a -> a) -> a -> Arbre coul val -> a 
dimensionGeneralisee _ v Feuille = v
dimensionGeneralisee f v (Noeud _ _ g d ) = f vg vd where
    vg = dimensionGeneralisee f v g
    vd = dimensionGeneralisee f v d

aux :: (Int,Bool) -> (Int,Bool) -> (Int,Bool)
aux (hg,pg) (hd,pd) = (h,p) where
    h = 1 + max hg hd
    p = hg == hd && pg && pd

estParfait :: Arbre c a -> Bool
estParfait a = snd (dimensionGeneralisee aux (0,True) a)

estParfait'' :: Arbre c a -> Bool
estParfait'' Feuille = True
estParfait'' a
    | (dimension (+) a) `mod` 2 == 1 = estParfait' a
    | otherwise = False where
        estParfait' :: Arbre c a -> Bool
        estParfait' (Noeud c v g d) = (dimension max g == dimension max d) &&
                                      (dimension min g == dimension min d)

parfait :: Int -> [(c, a)] -> Arbre c a
parfait 0 _ = Feuille
parfait __ [] = Feuille
parfait n xs =  Noeud  c v abrG abrD where
    abrG = parfait (n-1) (take o xs)
    abrD = parfait (n-1) (drop (o + 1) xs)
    c = fst(xs !! o)
    v = snd(xs !! o)
    o = (2^(n-1)) - 1

-- la fonction repeat :: a -> [a]
repeat' :: a -> [a]
repeat' a = iterate (\a -> a) a

aplatit :: Arbre c a -> [(c, a)]
aplatit abr = go : aplatit' abr where
    aplatit' :: Arbre c a -> (c, a)
    aplatit' Feuille = 
    aplatit' (Noeud c v g d) = 